<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;
use App\Form\RedditSearchType;
use App\Entity\RedditSearch;

class RedditController extends AbstractController
{
    /**
     * Processes the request.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $session = $this->get('session');
        $formData = new RedditSearch();

        // I'm pretty sure there is a better way to do this.
        if (!$request->isMethod('POST') && $session->get('codereview.filters')) {
            $formData = $session->get('codereview.filters');
        }

        $form = $this->createForm(RedditSearchType::class, $formData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session->set('codereview.filters', $formData);
        }

        try {
            $posts = $this->fetchPosts();
            $posts = $this->applyFilters($posts, $formData);
        } catch (Exception $e) {
            $posts = [];
        }

        return $this->render('reddit/index.html.twig', [
            'posts' => $posts,
            'filters' => $formData,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Filters the posts.
     *
     * @param array $posts
     * @param RedditSearch $formData
     * @return array
     */
    private function applyFilters(array $posts, RedditSearch $formData)
    {
        // Filtering by score
        $score = $formData->getScore();
        if (0 < $score) {
            $posts = array_filter($posts, function ($post) use ($score) {
                return isset($post->data->score) && $post->data->score >= $score;
            }, ARRAY_FILTER_USE_BOTH);
        }

        // Filtering by title
        $title = $formData->getTitle();
        if (strlen($title)) {
            $posts = array_filter($posts, function ($post) use ($title) {
                return isset($post->data->title) && (false !== stripos($post->data->title, $title));
            }, ARRAY_FILTER_USE_BOTH);
        }

        // Sorting
        $sorting = $formData->getSorting();
        if ('score' == $sorting) {
            usort($posts, [$this, 'sortPosts']);
        }

        return $posts;
    }

    /**
     * Returns Reddit posts.
     *
     * @return array
     */
    private function fetchPosts()
    {
        $client = new Client();
        $response = $client->get('https://www.reddit.com/r/PHP.json', [
            'headers' => [
                'User-Agent' => 'codereview/1.0', // Must specify User-Agent, Reddit returns 429 otherwise
                'Accept' => 'application/json',
            ],
        ]);

        $response = json_decode((string)$response->getBody());
        if (isset($response->data->children)) {
            return $response->data->children;
        }
        return [];
    }

    /**
     * Sorts the posts
     *
     * @param \stdClass $postA
     * @param \stdClass $postB
     * @return int
     */
    private function sortPosts($postA, $postB)
    {
        $scoreA = 0;
        if (isset($postA->data->score)) {
            $scoreA = $postA->data->score;
        }

        $scoreB = 0;
        if (isset($postB->data->score)) {
            $scoreB = $postB->data->score;
        }

        if ($scoreA == $scoreB) {
            return 0;
        }

        return ($scoreA < $scoreB) ? 1 : -1;
    }
}
