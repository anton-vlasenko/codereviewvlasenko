<?php

namespace App\Form;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\OptionsResolver\OptionsResolver;

class RedditSearchType extends AbstractType
{
    /**
     * Builds a reddit search form.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('score', IntegerType::class, ['required' => false])
            ->add('title')
            ->add('sorting', ChoiceType::class, [
                'choices' => [
                    'Default' => null,
                    'Score' => 'score',
                ]
            ])
            ->add('submit', SubmitType::class);
    }
}