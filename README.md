# /r/PHP

A simple web application that displays posts from /r/PHP.

### Installing

Just git clone it and run
```
cd project_directory
php composer.phar install
```

I've tested it with Apache and built-in PHP web-server. 